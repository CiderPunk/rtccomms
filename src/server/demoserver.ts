import * as Http from "http"
import * as Https from "https"
import * as fs from "fs"
import * as Express  from "express"
import * as Io from "socket.io"
import { DemoClient } from "./democlient"
import { DemoComms } from "../common/DemoComms"
import { IDemoServer, IDemoClient } from "./interfaces"

export class DemoServer implements IDemoServer{

  private readonly router:Express.Router
  private readonly app:Express.Express
  private readonly io:SocketIO.Server


  protected CreateHttpsServer(app:Express.Express){
    const privateKey = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/privkey.pem', 'utf8')
    const certificate = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/cert.pem', 'utf8')
    const ca = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/chain.pem', 'utf8')
    const credentials = {
      key: privateKey,
      cert: certificate,
      ca: ca
    }

    const httpsServer = Https.createServer(credentials, app)
    httpsServer.listen(443,()=>{
      console.log('listening on *:443')
    })
    return httpsServer
  } 

  protected CreateHttpServer(app:Express.Express, port:number = 80){
    const httpServer = Http.createServer(app)
    httpServer.listen(port,()=>{
      console.log('listening on *:' + port)
    })
    return httpServer
  } 

  
  protected readonly clients = new Map<string, DemoClient>()

  constructor(enableHttps:boolean = false, port:string = process.env.PORT || "80" ){
    this.app  = Express();
    //this.app.use('/src', Express.static('src'));
    this.app.use('/node_modules', Express.static('node_modules'));
    this.app.use(Express.static('public'));

    let server:any = null
    //https
    if (enableHttps){
      server = this.CreateHttpsServer(this.app)
      //create a simple redirect service
      const redirectApp = Express()
      redirectApp.get('*', (req,res)=>{
        res.redirect('https://' + req.headers.host + req.url);
      })
      this.CreateHttpServer(redirectApp,parseInt(port))
    }
    else{
      server = this.CreateHttpServer(this.app, parseInt(port))
    }

    //create socketio
    this.io = Io(server)
  
    this.io.on('connection', (socket:Io.Socket)=>{
      const client = new DemoClient(this, socket);
      this.clients.set(client.id, client)
      this.updateClientList();
    })

  }

  protected updateClientList() :void{
    this.io.emit(DemoComms.MessageType.ClientList, { clients: [...this.clients.keys()]  } as DemoComms.Payload.ClientList)
  }

  public dropClient(id: string):void {
    this.clients.delete(id)
    this.updateClientList();
  }
  
  public getClient(id: string):IDemoClient {
    return this.clients.get(id)
  }

}