import { IClient } from "../comms/server/interfaces";

export interface IDemoServer{
  getClient(id: string):IDemoClient;
  dropClient(id: string);

}

export interface IDemoClient extends IClient{

  
}