import * as Io from "socket.io"

import { BaseClient } from "../comms/server/baseclient";
import uuid = require("uuid");
import { DemoComms } from "../common/DemoComms";
import { IDemoServer, IDemoClient } from "./interfaces";

export class DemoClient extends BaseClient implements IDemoClient{

  public readonly id:string 

  public constructor(protected owner:IDemoServer, socket:Io.Socket){
    
    super(socket)
    this.id = uuid()
    //let the client know their ID...
    socket.emit(DemoComms.MessageType.ClientJoin, { yourKey: this.id } as DemoComms.Payload.ClientJoin)
    
    socket.on("disconnect",()=>{ 
      this.owner.dropClient( this.id) 
    })

    socket.on(DemoComms.MessageType.RequestConnect, (req:DemoComms.Payload.RequestConnect)=>{
      //find the target
      const target = this.owner.getClient(req.targetKey)
      if (target!== null){
        //initiate the connection
        this.startPeerConnect(target, true)    
      }
    }) 
  }

  public getPeerInfo():object{
    return { name: this.id } as DemoComms.Payload.DemoPeerInfo
  }

}