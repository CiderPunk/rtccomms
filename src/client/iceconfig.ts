export namespace Config{
  export const iceConfig = { iceServers:[{ 
    urls:"stun:starduel.ciderpunk.net:3478",
    credential: "test",
    username: "test"
  },
  {
    urls:"turn:starduel.ciderpunk.net:3478?transport=tcp",
    credential: "test",
    username: "test"
  } ]};

}