import { h, Component, ComponentChild } from "preact"
import { DemoComms } from "../common/DemoComms"
import * as io from "socket.io-client"
import { PeerConnectionManager } from "../comms/client/peerconnectionmanager"
import { DrawPad } from "./drawpad"
import { IPeerConnection, PeerConnectionState } from "../comms/client/clientinterfaces";


export interface ClientProps{
}

interface IPeer{
  id:string
  connectionState:PeerConnectionState
}

interface ClientState{
  myKey:string
  clients:IPeer[]
}

interface PlayerClientViewRowProps {
  peer:IPeer
  isYou:boolean
  onClick:(id:string)=>void
}

const connectionStateText = (state:PeerConnectionState) :string =>{
  switch (state){
    case PeerConnectionState.Connected: return "Connected"
    case PeerConnectionState.Connecting: return "Connecting"
    case PeerConnectionState.Unknown: return "Unknown"
    default: return "Not Connected"
  }
}

const ClientViewRow = (props:PlayerClientViewRowProps)=>{
  return(
    <li>
      {props.peer.id} <text> </text>
      {props.peer.connectionState == PeerConnectionState.Disconnected ? 
        !props.isYou? <button onClick={()=>{props.onClick(props.peer.id) }}>Connect</button>  :  <text>(YOU)</text>  :
        connectionStateText(props.peer.connectionState)}
    </li>
  )}

export class Client  extends Component<ClientProps, ClientState> {
  
  updateConnList(peerList:Array<IPeer> = null):void {
    peerList = (peerList === null ? this.state.clients : peerList)
    this.pcm.getConnections().map((pc:IPeerConnection)=>{ 
      const info = (pc.peerInfo as DemoComms.Payload.DemoPeerInfo)
      peerList.map((peer:IPeer)=>{ 
        if (peer.id === info.name ){
          peer.connectionState = pc.state
        }
      })
      //return (pc.peerInfo as DemoComms.Payload.DemoPeerInfo).name
    })  
    this.setState({clients: peerList })
  }

  public readonly socket:SocketIOClient.Socket
  public readonly pcm:PeerConnectionManager

  constructor(props:ClientProps){
    super(props)
    this.socket = io()
    this.socket.on("connect", ()=>{ 
      console.log("Connected")
    })
    this.state = { myKey: "", clients:new Array<IPeer>() }
    this.socket.on(DemoComms.MessageType.ClientJoin, (req:DemoComms.Payload.ClientJoin)=>{
        //server has assigned me a key
        this.setState({myKey : req.yourKey })
      })
    this.socket.on(DemoComms.MessageType.ClientList, (req:DemoComms.Payload.ClientList)=>{ 
        const peerList = req.clients.map(id=> ({ id:id , connectionState: PeerConnectionState.Disconnected } as IPeer))
        //this.setState({ clients:peerList})
        this.updateConnList(peerList)
      })
    this.pcm = new PeerConnectionManager(this.socket)
    this.pcm.onConnecting = 
      this.pcm.onDisconnected = 
      this.pcm.onConnected = (pc:IPeerConnection)=>{ 
        this.updateConnList(this.state.clients)
      }
  }

  render(props?: Readonly<ClientProps & import("preact").Attributes & { children?: import("preact").ComponentChildren; ref?: import("preact").Ref<any>; }>, state?: Readonly<ClientState>, context?: any): ComponentChild {
    return (
      <div class="columns">
        <div class="column">
          <p class="subtitle">Your key: {state.myKey}</p>
          <ul>
            { state.clients.map(c=>(<ClientViewRow  isYou={c.id == state.myKey} peer={c} onClick={(id:string)=>{ this.connectTo(id) }}/>))}
          </ul>
        </div>
        <div class="column">
          <DrawPad />
        </div>
      </div>

    )
  }

  connectTo(id: string) {
    console.log(`Connecting to ${id}`)
    this.socket.emit(DemoComms.MessageType.RequestConnect, {targetKey: id} as DemoComms.Payload.RequestConnect)
  }



}