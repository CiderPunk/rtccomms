import { h, Component, ComponentChild } from "preact"

interface DrawPadProps{
}

interface DrawPadState {
}

export class DrawPad extends Component<DrawPadProps, DrawPadState> {
  render(props?: Readonly<DrawPadProps>, state?: Readonly<DrawPadState>, context?: any): ComponentChild {
    return (<canvas width="100" height="100" class="drawpad" ></canvas>)
  }

  
}