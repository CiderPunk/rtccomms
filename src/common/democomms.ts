export namespace DemoComms{
  export namespace MessageType{
    export const ClientJoin = "clientjoin"
    export const ClientList = "clientlist"
    export const RequestConnect = "reqconn"
  }
  export namespace Payload{
    export interface ClientList{
      clients:Array<string>
    }

    export interface ClientJoin{
      yourKey:string
    }
    
    export interface RequestConnect{
      targetKey:string
    }


    export interface DemoPeerInfo{
      name:string
    }
  }


  

}