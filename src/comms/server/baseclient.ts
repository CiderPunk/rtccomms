import * as Io from "socket.io"
import { IClient } from "./interfaces";
import uuid = require("uuid");
import { SocketComms } from "../socketcomms";

export abstract class BaseClient implements IClient{

  public getPeerInfo():object{
    return null
  }

  public sendPeerSignal(other:IClient, key:string, data:object) {
    //check we have a record of this peer connection
    if (!this.peerConnections.has(key)){
      //add it
      this.peerConnections.set(key,other)
    }
    //transfer the data...
    this.socket.emit(SocketComms.MessageType.PeerSignal, { key: key, data: data} as SocketComms.Payload.PeerSignal)
  }

  readonly peerConnections:Map<string, IClient> 

  public startPeerConnect(target:IClient, isPrimary:boolean = true, key:string = null):void{
    //create a new key for this connection
    if (isPrimary){
      key = uuid()
      //prep the target for the connection
      target.startPeerConnect(this, false, key)
    }
    //create peer connection record
    this.peerConnections.set(key, target)
    //tell client to start connection process
    this.socket.emit(SocketComms.MessageType.InitConnection, { 
      key : key, 
      primary : isPrimary,
      peerInfo: target.getPeerInfo() 
    } as SocketComms.Payload.InitConnection)
    
  }

  constructor(protected readonly socket:Io.Socket){
    this.peerConnections = new Map<string, IClient>()
    //when we get a connection attempt...
    socket.on(SocketComms.MessageType.PeerSignal, (req:SocketComms.Payload.PeerSignal) => {
      //find the peer connection record
      const target = this.peerConnections.get(req.key)
      if (target !== null){
        //and send it...
        target.sendPeerSignal(this, req.key, req.data)
      }
    });

    socket.on("disconnect", ()=>{ 
      //remove peer connections to us
      this.peerConnections.forEach((client,key, map)=>{ client.closePeer(key)   })
    })
  }

  closePeer(key: string) {
    //called by disconnected peers to remove themselves from our list
    this.peerConnections.delete(key)
  }

}