

export interface IClient{
  getPeerInfo():object
  closePeer(key: string) : void
  startPeerConnect(target:IClient, primary:boolean, key:string):void
  sendPeerSignal(sender:IClient, key:string, data:object) :void
}
