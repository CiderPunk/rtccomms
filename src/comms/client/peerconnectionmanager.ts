import * as io from "socket.io-client"
import { Socket } from "net"
import { SocketComms } from "../socketcomms"
import { PeerConnection } from "./peerconnection"
import { IPeerConnectionManager, IPeerConnection } from "./clientinterfaces"

export class PeerConnectionManager implements IPeerConnectionManager{

  public onConnecting: (peerConn:IPeerConnection)=> void
  public onConnected: (peerConn:IPeerConnection) => void
  public onDisconnected: (peerConn:IPeerConnection)=> void

  connected(peerConn:IPeerConnection):void {
    if (this.onConnected){
      this.onConnected(peerConn)
    }
  }
  
  dropConnection(key: string) :void{
    const peerConn = this.peerConns.get(key)
    if (peerConn!= null && this.onDisconnected){
      this.onDisconnected(peerConn)
    }
    this.peerConns.delete(key)
  }

  protected readonly peerConns = new Map<string, PeerConnection>()

  public getConnections():Array<IPeerConnection>{ 
    return Array.from(this.peerConns.values())
  }

  public constructor(protected readonly socket:SocketIOClient.Socket){
    //signal - route to correct peer connection
    socket.on(SocketComms.MessageType.InitConnection, (req:SocketComms.Payload.InitConnection)=>{ 
      const peerConn =  new PeerConnection(this, req.primary, req.key, this.socket, req.peerInfo)
      this.peerConns.set(req.key, peerConn)
      if (this.onConnecting){
        this.onConnecting(peerConn)  
      }
    })  

    socket.on(SocketComms.MessageType.PeerSignal, (req:SocketComms.Payload.PeerSignal)=>{
      //try get the connection
      var peerConn = this.peerConns.get(req.key)
      if (!peerConn) {
        //try to create a new peer connection record if there is none
        //skipped this so we dont get peerconns without peerinfo
        //peerConn =  new PeerConnection(this, false, req.key, this.socket)
        //this.peerConns.set(req.key,peerConn)
        throw new Error("Unknown peer connection key")
      }     
      peerConn.signal(req.data)
    })
  }

}