
import * as io from "socket.io-client"
import Peer = require("simple-peer")
import { Config } from "../../client/iceconfig"
import { SocketComms } from "../socketcomms"
import { IPeerConnectionManager, IPeerConnection, PeerConnectionState } from "./clientinterfaces"


export class PeerConnection implements IPeerConnection{
  

  get state():PeerConnectionState { return this.connectionState }
  
  protected connectionState:PeerConnectionState
  protected readonly peer:Peer.Instance

  constructor(protected readonly owner: IPeerConnectionManager,
      primary:boolean, 
      public readonly key:string,
      protected readonly socket:SocketIOClient.Socket, 
      public readonly peerInfo:any = null){

    this.connectionState = PeerConnectionState.Connecting 
    this.peer = new Peer({
      initiator:primary,
      trickle: false,
      config: Config.iceConfig
    })
    
    //simple peer wants to send a signal, we forward it to the server via socket.io
    this.peer.on("signal", data=>{ 
      console.log("Sending signal data " + this.key)
      this.socket.emit(SocketComms.MessageType.PeerSignal, { key: this.key, data: data } as SocketComms.Payload.PeerSignal)
    })

    this.peer.on("connect", ()=>{
      console.log("Connected " + this.key)

      this.connectionState = PeerConnectionState.Connected
      //signal manager
      this.owner.connected(this)
    })
    this.peer.on("error",(err:any)=>{ 
      console.log("Error " + err.code + this.key)
      this.connectionState = PeerConnectionState.Disconnected
      this.owner.dropConnection(this.key)
    })
    this.peer.on("close",()=>{ 
      console.log("Disconnected " + this.key)
      this.connectionState = PeerConnectionState.Disconnected
      this.owner.dropConnection(this.key)
    })

  }
  public signal(data:any) : void{
    console.log("Recieved signal data "+ this.key)
    this.peer.signal(data)
  }


}