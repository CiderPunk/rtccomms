export interface IPeerConnectionManager{
  connected(peerConn: IPeerConnection)
  dropConnection(key:string)
  getConnections():Array<IPeerConnection>
  onConnecting: (peerConn:IPeerConnection) => void
  onConnected: (peerConn:IPeerConnection) => void
  onDisconnected: (peerConn:IPeerConnection) => void
}

export interface IPeerConnection{
  peerInfo:any
  readonly state:PeerConnectionState   
}

export interface ICommsBuffer{

}

export enum PeerConnectionState{
  
  Disconnected,
  Connecting,
  Connected,
  Unknown,
  
}